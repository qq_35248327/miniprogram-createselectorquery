Page({
  showFields(root, selector, tag) {
    root.createSelectorQuery().select(selector)
      .fields(
        { id: true, dataset: true },
        res => console.log(tag, JSON.stringify(res))
      )
      .exec()
  },

  onTap() {
    this.showFields(wx, '#page-view', 'sync wx')
    this.showFields(this, '#page-view', 'sync this')

    setTimeout(() => {
      this.showFields(wx, '#page-view', 'async wx')
      this.showFields(this, '#page-view', 'async this')
    }, 1000)

    wx.navigateTo({ url: '/pages/another/another' })
  }
})