Component({
  methods: {
    showFields(root, selector, tag) {
      root.createSelectorQuery().select(selector)
        .fields(
          { id: true, dataset: true },
          res => console.log(tag, JSON.stringify(res))
        )
        .exec()
    },

    onTap() {
      this.showFields(wx, '#my-component-view', 'wx')
      this.showFields(this, '#my-component-view', 'this')
    }
  }
})